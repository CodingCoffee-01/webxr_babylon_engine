var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true,  disableWebGL2Support: false}); };
var createScene = function () {
    var scene = new BABYLON.Scene(engine);

    var camera=new BABYLON.ArcRotateCamera("camera",1,1,13,BABYLON.Vector3.Zero(),scene);

    camera.setTarget(new BABYLON.Vector3(0,4,0));

    camera.attachControl(canvas, true);

    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    light.intensity = 0.7;

  BABYLON.SceneLoader.ImportMesh("", "../scenes/Dude/", "Dude.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      var mesh = newMeshes[0];
    var skeleton = skeletons[0];
    mesh.scaling = new BABYLON.Vector3(0.1,0.1,0.1);

    var t = 0;

    scene.beforeRender = function () {

      t += .1;

      skeleton.bones[7].setAbsolutePosition(new BABYLON.Vector3(0, 6.5, 0), mesh);

      mesh.position.x = Math.sin(t) * .1;

    }

  });

    return scene;
};
        window.initFunction = async function() {


            var asyncEngineCreation = async function() {
                try {
                return createDefaultEngine();
                } catch(e) {
                console.log("the available createEngine function failed. Creating the default engine instead");
                return createDefaultEngine();
                }
            }

            window.engine = await asyncEngineCreation();
if (!engine) throw 'engine should not be null.';
startRenderLoop(engine, canvas);
window.scene = createScene();};
initFunction().then(() => {sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});
