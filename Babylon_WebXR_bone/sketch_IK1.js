var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true,  disableWebGL2Support: false}); };
var createScene = function () {

  var scene = new BABYLON.Scene(engine);

    var camera=new BABYLON.ArcRotateCamera("camera",0,1,20,BABYLON.Vector3.Zero(),scene);

    camera.setTarget(new BABYLON.Vector3(0,4,0));

    camera.attachControl(canvas, true);

    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    light.intensity = 0.7;

  var sphere = BABYLON.MeshBuilder.CreateSphere('', {diameter:.5}, scene);

  BABYLON.SceneLoader.ImportMesh("", "../scenes/Dude/", "Dude.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      var mesh = newMeshes[0];
    var skeleton = skeletons[0];
    mesh.scaling = new BABYLON.Vector3(0.1,0.1,0.1);
    mesh.position = new BABYLON.Vector3(0, 0, 0);

    var animation = scene.beginAnimation(skeletons[0], 0, 100, true, 1.0);

    var t1 = 0;
    var t2 = 0;

    var lookAtCtl = new BABYLON.BoneLookController(mesh, skeleton.bones[7], sphere.position, {adjustYaw:Math.PI*.5, adjustRoll:Math.PI*.5});

    var boneAxesViewer = new BABYLON.Debug.BoneAxesViewer(scene, skeleton.bones[7], mesh);

    scene.registerBeforeRender(function () {

      t1 += .02;
      t2 += .03;

      sphere.position.x = 12 * Math.sin(t1);
      sphere.position.y = 6 + 6 * Math.sin(t2);
      sphere.position.z = -6;

      lookAtCtl.update();

      boneAxesViewer.update();

    });


  });

    return scene;
};
        window.initFunction = async function() {


            var asyncEngineCreation = async function() {
                try {
                return createDefaultEngine();
                } catch(e) {
                console.log("the available createEngine function failed. Creating the default engine instead");
                return createDefaultEngine();
                }
            }

            window.engine = await asyncEngineCreation();
if (!engine) throw 'engine should not be null.';
startRenderLoop(engine, canvas);
window.scene = createScene();};
initFunction().then(() => {sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});
