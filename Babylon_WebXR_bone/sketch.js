var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true,  disableWebGL2Support: false}); };
var delayCreateScene = function () {

    var scene = new BABYLON.Scene(engine);

    BABYLON.SceneLoader.ImportMesh("", "../scenes/Dude/", "Dude.babylon", scene,
        function (meshes, particleSystems, skeletons) {
            scene.createDefaultCameraOrLight(true, true, true);
            scene.createDefaultEnvironment();

            scene.beginAnimation(skeletons[0], 0, 100, true, 1.0);
        }
    );

    return scene;
};
        window.initFunction = async function() {


            var asyncEngineCreation = async function() {
                try {
                return createDefaultEngine();
                } catch(e) {
                console.log("the available createEngine function failed. Creating the default engine instead");
                return createDefaultEngine();
                }
            }

            window.engine = await asyncEngineCreation();
if (!engine) throw 'engine should not be null.';
startRenderLoop(engine, canvas);
window.scene = delayCreateScene();};
initFunction().then(() => {sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});
