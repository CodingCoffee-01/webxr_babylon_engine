var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true,  disableWebGL2Support: false}); };
var createScene = function () {
    var scene = new BABYLON.Scene(engine);

    var camera=new BABYLON.ArcRotateCamera("camera",0,1,25,BABYLON.Vector3.Zero(),scene);

    camera.setTarget(new BABYLON.Vector3(0,4,0));

    camera.attachControl(canvas, true);

    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    light.intensity = 0.7;

  var oldgui = document.querySelector("#datGUI");
  if (oldgui != null){
    oldgui.remove();
  }

  var gui = new dat.GUI();
  gui.domElement.style.marginTop = "100px";
  gui.domElement.id = "datGUI";


  var target = BABYLON.MeshBuilder.CreateSphere('', { diameter: 5 }, scene);
  var poleTarget = BABYLON.MeshBuilder.CreateSphere('', {diameter: 2.5}, scene);


  BABYLON.SceneLoader.ImportMesh("", "../scenes/Dude/", "Dude.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      var mesh = newMeshes[0];
    var skeleton = skeletons[0];
    mesh.scaling = new BABYLON.Vector3(0.1,0.1,0.1);
    mesh.position = new BABYLON.Vector3(0, 0, 0);

    var animation = scene.beginAnimation(skeletons[0], 0, 100, true, 1.0);

    var t = 0;

    poleTarget.position.x = 0;
    poleTarget.position.y = 100;
    poleTarget.position.z = -50;

    target.parent = mesh;
    poleTarget.parent = mesh;

    var ikCtl = new BABYLON.BoneIKController(mesh, skeleton.bones[14], {targetMesh:target, poleTargetMesh:poleTarget, poleAngle: Math.PI});

    ikCtl.maxAngle = Math.PI * .9;

    var bone1AxesViewer = new BABYLON.Debug.BoneAxesViewer(scene, skeleton.bones[14], mesh);
    var bone2AxesViewer = new BABYLON.Debug.BoneAxesViewer(scene, skeleton.bones[13], mesh);

    gui.add(ikCtl, 'poleAngle', -Math.PI, Math.PI);
    gui.add(ikCtl, 'maxAngle', 0, Math.PI);
    gui.add(poleTarget.position, 'x', -100, 100).name('pole target x');
    gui.add(poleTarget.position, 'y', -100, 100).name('pole target y');
    gui.add(poleTarget.position, 'z', -100, 100).name('pole target z');

    scene.registerBeforeRender(function () {

      var bone = skeleton.bones[14];

      t += .03;

      var dist = 2 + 12*Math.sin(t);

      target.position.x = -20;
      target.position.y = 40 + 40 * Math.sin(t);
      target.position.z = -30 + 40 * Math.cos(t);

      ikCtl.update();

      //mesh.rotation.y += .01;

      bone1AxesViewer.update();
      bone2AxesViewer.update();

    });

  });


    return scene;
};
        window.initFunction = async function() {


            var asyncEngineCreation = async function() {
                try {
                return createDefaultEngine();
                } catch(e) {
                console.log("the available createEngine function failed. Creating the default engine instead");
                return createDefaultEngine();
                }
            }

            window.engine = await asyncEngineCreation();
if (!engine) throw 'engine should not be null.';
startRenderLoop(engine, canvas);
window.scene = createScene();};
initFunction().then(() => {sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});
