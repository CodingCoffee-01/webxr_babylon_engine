var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true,  disableWebGL2Support: false}); };
var createScene = function () {

    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);

    // This creates and positions a free camera (non-mesh)
    var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 5, -10), scene);

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'sphere' shape. Params: name, subdivs, size, scene
    var sphere = BABYLON.Mesh.CreateSphere("sphere1", 16, 2, scene);

    // Move the sphere upward 1/2 its height
  sphere.scaling = new BABYLON.Vector3(0.1, 0.1, 0.1);
  var materialSphere = new BABYLON.StandardMaterial("texture1", scene);
  materialSphere.diffuseColor = BABYLON.Color3.Red();
  sphere.material = materialSphere;

    // Our built-in 'ground' shape. Params: name, width, depth, subdivs, scene
    var ground = BABYLON.Mesh.CreateGround("ground1", 100, 100, 1, scene);

  var dude;
  var skeleton;
  BABYLON.SceneLoader.ImportMesh("him", "../scenes/Dude/", "Dude.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      dude = newMeshes[0];
    skeleton = skeletons[0];
  //	console.log(skeleton.bones)
      dude.rotation.y = Math.PI;
    dude.scaling = new BABYLON.Vector3(0.02,0.02,0.02);
      dude.position = new BABYLON.Vector3(0, 0, 0);
    skeleton.position = new BABYLON.Vector3(0, 0, 0);
    skeleton.scaling = new BABYLON.Vector3(0.02,0.02,0.02);

    sphere.attachToBone(skeleton.bones[34], dude);
    sphere.scaling = new BABYLON.Vector3(5, 5, 5);

      scene.beginAnimation(skeletons[0], 0, 100, true, 1.0);

    scene.registerBeforeRender(function () {
      dude.position.z+= 0.005;
    });
  });


    return scene;

};

        window.initFunction = async function() {


            var asyncEngineCreation = async function() {
                try {
                return createDefaultEngine();
                } catch(e) {
                console.log("the available createEngine function failed. Creating the default engine instead");
                return createDefaultEngine();
                }
            }

            window.engine = await asyncEngineCreation();
if (!engine) throw 'engine should not be null.';
startRenderLoop(engine, canvas);
window.scene = createScene();};
initFunction().then(() => {sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});
